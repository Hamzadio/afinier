#ifndef CSTATIONMETEO_H
#define CSTATIONMETEO_H
#include <QtSerialPort>
#include <iostream>

using namespace std;
class CStationMeteo
{
private:
    QSerialPort PortSerie; //declaration de l'instance
public:
    CStationMeteo(); // constructeur de la classe (port serie)
    float LectureDonneesMeteov1 (); // declaration de la classe (récéption des donné meteo)

   bool LectureDonneesMeteov2 (float&,float&,float&); // declaration de la classe (récéption des donné meteo)
};

#endif // CSTATIONMETEO_H
