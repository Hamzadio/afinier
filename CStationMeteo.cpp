#include "CStationMeteo.h"

CStationMeteo::CStationMeteo()
{
    PortSerie.setBaudRate(QSerialPort::Baud9600);
    PortSerie.setDataBits(QSerialPort::Data8);
    PortSerie.setParity(QSerialPort::NoParity);
    PortSerie.setStopBits(QSerialPort::OneStop);
    PortSerie.setFlowControl(QSerialPort::HardwareControl); //on utilise le rts cts (docklyght)
    PortSerie.setPortName("COM14");
    PortSerie.open(QIODevice::ReadOnly);//Qiodevice=périphérique(entré/sortie) lire seulement
    if (PortSerie.isOpen())
    {
        PortSerie.setRequestToSend(false); //désactivation du rts (on dit a la station meteo de se taire)
        PortSerie.clear(); // ça vide le buffer associer au port serie
    }
}

  float CStationMeteo::LectureDonneesMeteov1 ()
{

  float temperature =0 ;
  if (PortSerie.isOpen())
  {
      PortSerie.setRequestToSend(true); //réactivation du rts (on dit a la station meteo de se remettre en marche et de nous envoyer la tramme)
      QByteArray trame; // Qbyte arret = tableau d'octet
      while (PortSerie.waitForReadyRead(150))// timeout inferieur a la periode d'envoi (200 ms)
          trame=trame+PortSerie.readAll();// a chaque fois qu'il y a des octet qui arrive on les prend et on les lit
        // ON SORT DE LA BOUCLE SI ON A RIEN REçU PENDANT 101 MS
      PortSerie.setRequestToSend(false); // l'arreter parce que on a l
      if ((trame.size()==17 )&& (trame.at(0)== 0x55))
      {

          /* si la trame est correcte on
            va verifiersi il y a 17 octer avec le et (&&) mais aussi si lans le premier
            octet de la tramme les numero sont 55 0x55(en hex) toujour en hex */
         //cout << hex << (int) trame.at(7)<< (int)trame.at(8) << endl;
        temperature=((char) trame.at(7)*256 + (unsigned char)trame.at(8))/(float)16;
      }


  }

  return temperature;
}

     bool CStationMeteo::LectureDonneesMeteov2 (float& temperature,float& pression ,float& vitessVent) // declaration de la classe (récéption des donné meteo)
     {
         bool resultat=true;
         if (PortSerie.isOpen())
         {
             PortSerie.setRequestToSend(true); //réactivation du rts (on dit a la station meteo de se remettre en marche et de nous envoyer la tramme)
             QByteArray trame; // Qbyte arret = tableau d'octet
             while (PortSerie.waitForReadyRead(150))// timeout inferieur a la periode d'envoi (200 ms)
                 trame=trame+PortSerie.readAll();// a chaque fois qu'il y a des octet qui arrive on les prend et on les lit
               // ON SORT DE LA BOUCLE SI ON A RIEN REçU PENDANT 101 MS
             PortSerie.setRequestToSend(false); // l'arreter parce que on a l
             if ((trame.size()==17 )&& (trame.at(0)== 0x55))
             {

                 /* si la trame est correcte on
                   va verifiersi il y a 17 octer avec le et (&&) mais aussi si lans le premier
                   octet de la tramme les numero sont 55 0x55(en hex) toujour en hex */
                //cout << hex << (int) trame.at(7)<< (int)trame.at(8) << endl;
               temperature=((char) trame.at(7)*256 + (unsigned char)trame.at(8))/(float)16;
              pression=((char) trame.at(9)*256 + (unsigned char)trame.at(10))/(float)16;
               temperature=((char) trame.at(3)*256 + (unsigned char)trame.at(4))/(float)16;
             }
             else{
                 resultat=false;
             }

        }
         else{
             resultat= false;
         }
         return resultat;
}














